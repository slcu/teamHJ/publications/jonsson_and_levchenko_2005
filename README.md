## Introduction

This repository includes model init and solver files for files for reproducing 
simulations in the publication 

Henrik Jönsson and Andre Levchenko (2005)
An Explicit Spatial Model of Yeast Microcolony Growth
<i>Multiscale Model. Simul.</i>, 3(2), 346–361.
https://doi.org/10.1137/040603255

## Running Simulations

Simulations are running using the Organism c++ software 
(see below for download instructions). It is terminal based and requires a model, 
init and solver file as input. Example model and configuration files for running 
the simulations can be found in the main repository directory.

### Model file

A model for budding yeast colony growth is defined in <tt>yeast.model</tt>. The model
define reactions for growth (SphereBud::GrowthTruncNeigh - Eqs. 3.1-3.3), cell-cell
interactions (SphereBud::springAsymmetric - Eqs. 3.8-3.9), cell division 
(SphereBud::divisionCellDirection - Section 3.3), and neighbourhood definition 
(SphereBud::neighborhoodDistance - Section 3.4).

### Init file

The example init file <tt>yeast.init</tt> have two cells, each defined by positions 
and sizes: X Y x y R r, where X,Y,R (x,y,r) defines the mother (bud) variables.

Note, all reactions in the model are independent on dimensions, so initiating cells in 3D
(and update the number of topology variables in the model file) will allow for 3D simulations.

### Solver file

<tt>yeast.rk</tt> defines the solver used, time of the simulation, and number of 
states to be printed during simulations.

### Run and plot a yeast colony growth model

To run a simulation 

<tt>$ ORGANISM/bin/simulator yeast.model yeast.init yeast.rk > yeast.data</tt>

This will save the output in the <tt>yeast.data</tt> file, which can be
visualised using

<tt>$ ORGANISM/tools/plot/bin/newman -shape buddingYeast yeast.data</tt>

See the <tt>newman</tt> documentation for how to change visualisation
parameters and generate e.g. image/movie output.

### Organism (and tissue) software - overview

The Organism-Tissue software is written in c++ and is used to simulate
biological models in multicellular systems, where cell (and wall for tissue)
compartments are included. Several different molecular reactions and
mechanical update rules, ODE and stochastic solvers, and parameter optimisation methods
are defined within the software.
For the simulations used in this work systems of ordinary differential
equations are used to simulate the growth and division dynamics of the system.

### Organism - downloading and compiling

The open source software is available through download via the Sainsbury Laboratory 
gitlab repository

https://gitlab.com/slcu/teamhj/organism

Once downloaded, the software can be compiled via a Makefile. In the
<tt>ORGANISM/src/</tt> directory of your download (ORGANISM is the main folder), 
just type

<tt>$ cd ORGANISM/src<br>
$ make</tt>

and the software should compile provided that all dependencies are
fulfilled (see Organism documentation for details). Similar, the visualisation
tool <tt>newman</tt> is compiled by

<tt>$ cd ORGANISM/tools/plot/src<br>
$ make</tt>

Note, the visualisation is based on openGL and requires some more libraries 
installed.


## User inputs and differences with original version

The files in this repository can be used to generate the results of the 2005 paper,
with some effort from a user. Much of the results was presented as statistics from 
multiple runs per parameter configuration, using some random initial conditions. 
Scripts to run multiple runs and collect statistics are not provided in this repository. 
An available tool for scanning parameters in Organism model files
is the <tt>auto</tt> tool provided in the ORGANISM/tools directory. If only one print point
is provided in the <tt>yeast.rk</tt> file, only the final time point is saved and the colony
shape metrics should be straightforward to extract from these output files (please let 
us know if you would need assistance with this or extracting alternative metrics).
Initial condition files for stsatistics can be generated as described in Appendix B in the paper.

Since the publication of the paper, the Organism software have changed and there are some minor differences in the 
current model implementation compared with the original paper:

<p> The neighbour-dependent factor (Eq. 3.10) is now implemented as (k_{neigh}N_{neigh})^(-p_{neigh}).</p>
<p> In the paper, the r_{max} had a small random contribution (to make divisions asynchronous), now
a random contribution is provided for the initial size of the buds.</p>

## CONTACT

The software is mainly developed at the Sainsbury Laboratory, University of Cambridge. 

For questions please email henrik.jonsson@slcu.cam.ac.uk
